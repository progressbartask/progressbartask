(function () {
  'use strict';

  /**
   * @ngdoc service
   * @name task.constant:app
   *
   * @description
   *
   */
  angular
    .module('task')
    .constant('APP_CONSTANTS', {
      BAR_URL: 'http://pb-api.herokuapp.com/bars'
    });
}());
