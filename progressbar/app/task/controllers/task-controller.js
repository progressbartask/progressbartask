(function () {
  'use strict';

  /**
   * @ngdoc object
   * @name task.controller:TaskCtrl
   *
   * @description
   *
   */
  angular
    .module('task')
    .controller('TaskCtrl', TaskCtrl);

  TaskCtrl.$inject = ['BarLoader', '$log'];

  function TaskCtrl(BarLoader, $log) {
    var vm = this,
        tmpObj;
    vm.ctrlName = 'TaskCtrl';
    vm.bars = [];
    vm.buttons = [];
    vm.barOptions = [];
    vm.limit = 100;
    vm.selectedProgressBar = 0;
    vm.selectedOption = null;
    vm.updateValue = updateValue;
    vm.loadData = loadData;

    function updateValue(val) {
      vm.bars[vm.selectedOption.id] += val;
      if (vm.bars[vm.selectedOption.id] < 0) {
        vm.bars[vm.selectedOption.id] = 0;
      }
    }

    function loadData() {
      BarLoader
      .getData()
      .then(function (data) {
        vm.bars = data.bars;
        vm.buttons = data.buttons;
        vm.limit = data.limit;
        vm.barOptions = _.map(vm.bars, function (item, i) {
          tmpObj = {
            value: item,
            id: i,
            label: 'Progress Bar No ' + (i + 1)
          };
          return tmpObj;
        });
        vm.selectedOption = vm.barOptions[0];
      })
      .catch(function (error) {
        $log.error(error);
      });
    }

    function init() {
      vm.loadData();
    }

    init();
  }
}());
