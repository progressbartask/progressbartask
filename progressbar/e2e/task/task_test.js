/* global describe, beforeEach, it, browser, expect */
'use strict';

var TaskPagePo = require('./task.po');

describe('Task page', function () {
  var taskPage;

  beforeEach(function () {
    taskPage = new TaskPagePo();
    browser.get('/#/task');
  });

  it('should say TaskCtrl', function () {
    expect(taskPage.heading.getText()).toEqual('task');
  });
});
