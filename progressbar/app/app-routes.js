(function () {
  'use strict';

  angular
    .module('progressbar')
    .config(config);

  function config($urlRouterProvider) {
    $urlRouterProvider.otherwise('/task');
  }
}());
