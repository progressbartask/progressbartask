/* global describe, beforeEach, it, expect, inject, module */
'use strict';

describe('BarLoader', function () {
  var factory, httpBackend;

  beforeEach(module('task'));

  beforeEach(inject(function (BarLoader, $httpBackend) {
    factory = BarLoader;
    httpBackend = $httpBackend;
  }));

  it('should have getData be defined', function () {
    expect(factory.getData).toBeDefined();
  });

  it('getData should return data on success', function () {
    
    httpBackend.when('GET', 'http://pb-api.herokuapp.com/bars')
      .respond({
        data:{
          buttons: [24, 5, -12, -36],
          bars: [78, 42, 45, 20],
          limit: 190
        }
      });
    factory.getData()
      .then(function (res) {
        expect(res.data.limit).toEqual(190);
      });
    httpBackend.flush();
  });

  it('getData should return error on failure', function () {
    
    httpBackend.when('GET', 'http://pb-api.herokuapp.com/bars')
      .respond(401);
    factory.getData()
      .catch(function (error) {
        expect(error).toEqual(undefined);
      });
    httpBackend.flush();
  });
});
