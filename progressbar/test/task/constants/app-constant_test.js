/* global describe, beforeEach, it, expect, inject, module */
'use strict';

describe('APP_CONSTANTS', function () {
  var constant;

  beforeEach(module('task'));

  beforeEach(inject(function (APP_CONSTANTS) {
    constant = APP_CONSTANTS;
  }));

  it('should be defined', function () {
    expect(constant).toBeDefined();
  });

  it('BAR_URL should equal http://pb-api.herokuapp.com/bars', function () {
    expect(constant.BAR_URL).toBe('http://pb-api.herokuapp.com/bars');
  });
});
