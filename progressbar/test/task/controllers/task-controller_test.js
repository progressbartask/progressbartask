/* global describe, beforeEach, it, expect, inject, module */
'use strict';

describe('TaskCtrl', function () {
  var ctrl, BarLoader, httpBackend;

  beforeEach(module('task'));

  beforeEach(inject(function ($rootScope, $controller, $httpBackend, _BarLoader_) {
  	httpBackend = $httpBackend;
  	BarLoader = _BarLoader_;
    ctrl = $controller('TaskCtrl', {
    	BarLoader: BarLoader
    });
  }));

  it('should have ctrlName as TaskCtrl', function () {
    expect(ctrl.ctrlName).toEqual('TaskCtrl');
  });

  it('should have bars as zero length', function () {
    expect(ctrl.bars.length).toEqual(0);
  });

  it('should have selectedOption as undefined', function () {
    expect(ctrl.selectedOption).toEqual(null);
  });

  it('loadData should be defined', function () {
    expect(ctrl.loadData).toBeDefined();
  });

  it('loadData should return data on success', function () {
    httpBackend.when('GET', 'http://pb-api.herokuapp.com/bars')
      .respond(
        {
          buttons: [24, 5, -12, -36],
          bars: [78, 42, 45, 20],
          limit: 190
      });
    ctrl.loadData();
    httpBackend.flush();
    expect(ctrl.limit).toEqual(190);
  });

  it('loadData should return error on failure', function () {
    httpBackend.when('GET', 'http://pb-api.herokuapp.com/bars')
      .respond(401);
    ctrl.loadData();
    httpBackend.flush();
    expect(ctrl.limit).toEqual(100);
  });

  it('should update bars value when updateValue called', function () {
  	ctrl.selectedOption = {
  		id: 0,
  		label: 'Progress Bar No 1'
  	};
  	ctrl.bars = [10];
  	ctrl.updateValue(10);
    expect(ctrl.bars[0]).toEqual(20);
  });

  it('should update bars value to 0 when updateValue is negetive', function () {
  	ctrl.selectedOption = {
		id: 0,
		label: 'first progressbar'
  	};
  	ctrl.bars = [10];
  	ctrl.updateValue(-30);
    expect(ctrl.bars[0]).toEqual(0);
  });
});
