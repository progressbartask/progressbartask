(function () {
  'use strict';

  /* @ngdoc object
   * @name progressbar
   * @description
   *
   */
  angular
    .module('progressbar', [
      'ui.router',
      'ui.bootstrap',
      'task'
    ]);
}());
