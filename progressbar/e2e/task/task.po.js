/* global element, by */
'use strict';

function TaskPage() {
  this.heading = element(by.tagName('h2'));
}

module.exports = TaskPage;
