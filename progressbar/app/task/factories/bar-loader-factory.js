(function () {
  'use strict';

  /**
   * @ngdoc service
   * @name task.factory:BarLoader
   *
   * @description
   *
   */
  angular
    .module('task')
    .factory('BarLoader', BarLoader);

  BarLoader.$inject = ['$http', 'APP_CONSTANTS', '$q'];

  function BarLoader($http, APP_CONSTANTS, $q) {
    var BarLoaderBase = {
      getData: getData
    };

    return BarLoaderBase;

    function getData() {
      var deferred = $q.defer();
      $http({
        method: 'GET',
        url: 'http://pb-api.herokuapp.com/bars'
      })
      .then(function (response) {
        deferred.resolve(response.data);
      })
      .catch(function (error) {
        deferred.reject(error.message || error.data);
      });
      return deferred.promise;
    }
  }
}());
