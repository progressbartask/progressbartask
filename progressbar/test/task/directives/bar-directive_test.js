/* global describe, beforeEach, it, expect, inject, module */
'use strict';

describe('bar', function () {
  var scope,
      element,
      timeout;

  beforeEach(module('task', 'task/directives/bar-directive.tpl.html'));

  beforeEach(inject(function ($compile, $rootScope, _$timeout_) {
    timeout = _$timeout_;
    scope = $rootScope.$new();
    element = $compile(angular.element('<bar limit="limit" current-progress="barItem"></bar>'))(scope);
  }));

  it('should have correct text', function () {
    scope.limit = 200;
    scope.barItem = 100;
    scope.$apply();
    expect(element.isolateScope().bar.name).toEqual('bar');
    timeout( function() {
      scope.barItem = 250;
      scope.$apply();
      timeout(function(){
        scope.barItem = -10;
        scope.$apply();
      });
      timeout.flush();
    }, 1000);
    timeout.flush();
  });
});
