(function () {
  'use strict';

  /* @ngdoc object
   * @name task
   * @description
   *
   */
  angular
    .module('task', [
      'ui.router'
    ]);
}());
