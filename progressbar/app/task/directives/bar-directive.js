(function () {
  'use strict';

  /**
   * @ngdoc directive
   * @name task.directive:bar
   * @restrict EA
   * @element
   *
   * @description
   *
   * @example
     <example module='task'>
       <file name='index.html'>
        <bar></bar>
       </file>
     </example>
   *
   */
  angular
    .module('task')
    .directive('bar', bar);

  function bar() {
    return {
      restrict: 'EA',
      scope: {
        limit: '=',
        progress: '=currentProgress'
      },
      templateUrl: 'task/directives/bar-directive.tpl.html',
      replace: true,
      controllerAs: 'bar',
      bindToController: true,
      controller: BarCtrl,
      link: function (scope, element, attrs, ctrl) {
        /* jshint unused:false */
        /* eslint "no-unused-vars": [2, {"args": "none"}] */
        var progressBarElement = element.find('.progress-bar');

        function valueWatcher() {
          return ctrl.value;
        }
        scope.$watch(valueWatcher, function (val) {
          if (val < 0) {
            ctrl.value = 0;
          } else if (val > ctrl.limit) {
            ctrl.value = ctrl.limit;
          }
          progressBarElement.css({
            width: ctrl.value / ctrl.limit * 100 + '%'
          });
        });
      }
    };
  }

  BarCtrl.$inject = ['$timeout', '$scope'];

  function BarCtrl($timeout, $scope) {
    var vm = this;
    vm.name = 'bar';
    vm.value = 0;
    vm.isLimitCrossed = false;

    function progressWatcher() {
      return vm.progress;
    }

    $timeout(function () {
      vm.value = vm.progress;
      $scope.$watch(progressWatcher, function (value) {
        vm.value = value;
      });
    }, 100);

    vm.getPercent = function () {
      if (vm.progress > vm.limit) {
        vm.isLimitCrossed = true;
      } else {
        vm.isLimitCrossed = false;
      }
      return Math.ceil(vm.progress / vm.limit * 100) + '%';
    };
  }
}());
