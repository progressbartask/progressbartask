(function () {
  'use strict';

  angular
    .module('task')
    .config(config);

  function config($stateProvider) {
    $stateProvider
      .state('task', {
        url: '/task',
        templateUrl: 'task/views/task.tpl.html',
        controller: 'TaskCtrl',
        controllerAs: 'task'
      });
  }
}());
